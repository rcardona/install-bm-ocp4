#!/bin/bash

source ocp4_install_env.sh

sudo dnf makecache

# install kvm
sudo dnf install qemu-kvm libvirt virt-install virt-manager virt-install libguestfs-tools-c syslinux tftp-server dhcp-server -y
sudo dnf install podman skopeo httpd haproxy bind bind-utils net-tools nfs-utils rpcbind wget tree git lvm2 lvm2-libs firewalld jq -y
