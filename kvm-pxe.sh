# ////////////     0 - installing packges start  ////////////
# # First, update the YUM package repository cache
# $ sudo dnf makecache
#
# # install kvm
# $ sudo dnf install qemu-kvm libvirt virt-install virt-manager virt-install libguestfs-tools-c syslinux tftp-server dhcp-server -y
# $ sudo dnf install podman skopeo httpd haproxy bind bind-utils net-tools nfs-utils rpcbind wget tree git lvm2 lvm2-libs firewalld jq -y
#
# ////////////     0 - installing packges end  ////////////




# ////////////     1 - configuring kvm start  ////////////
# # check kvm capabilities
# $ grep -E --color 'vmx|svm' /proc/cpuinfo
#
# # create lvm
# $ sudo lsblk
# $ sudo pvcreate /dev/sdb
# $ sudo vgextend /dev/rhel /dev/sdb
# $ sudo lvcreate -l 100%FREE -n lv-kvm /dev/rhel
# $ sudo mkfs.xfs /dev/rhel/lv-kvm
# $ sudo blkid
# $ sudo vim /etc/fstab
# $ sudo mount -a
#
# # show active interfaces
# $ sudo nmcli c s --active
#
# # create network bridge
# $ sudo nmcli con add ifname br0 type bridge con-name br0
# $ sudo mcli con add type bridge-slave ifname eno1 master br0
#
# # create network definion file
# $ cat > /tmp/bridge.xml << EOF
# <network>
#   <name>net-ocp4</name>
#   <forward mode="bridge"/>
#   <bridge name="br0" />
# </network>
# EOF

# # define network
# $ sudo virsh net-define /tmp/bridge.xml
# $ sudo virsh net-start br0
# $ sudo virsh net-autostart br0
# ////////////     1 - configuring kvm end  ////////////




////////////     2 - gathering artifacts start  ////////////
# download artifacts
$ sudo mkdir -p /artifacts
$ cd /artifacts
$ export OCPVERSION="4.4.3"
$ export OCPREPORHCOS="https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/latest/latest"
$ sudo wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-linux.tar.gz
$ sudo wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-client-linux.tar.gz
$ sudo wget ${OCPREPORHCOS}/rhcos-${OCPVERSION}-x86_64-metal.x86_64.raw.gz
$ sudo wget ${OCPREPORHCOS}/rhcos-${OCPVERSION}-x86_64-installer-initramfs.x86_64.img
$ sudo wget ${OCPREPORHCOS}/rhcos-${OCPVERSION}-x86_64-installer-kernel-x86_64
$ sudo wget ${OCPREPORHCOS}/rhcos-${OCPVERSION}-x86_64-qemu.x86_64.qcow2.gz
////////////     2 - gathering artifacts end  ////////////




////////////     3 - pxe server configuration start   ////////////
# Configuration PXE Server File
$ sudo mkdir /var/lib/tftpboot/pxelinux
$ sudo cp -r /usr/share/syslinux/* /var/lib/tftpboot/pxelinux

# Configuration PXE Server File
$ sudo mkdir -p /var/lib/tftpboot/pxelinux/pxelinux.cfg
$ sudo touch /var/lib/tftpboot/pxelinux.cfg/default

# now edit PXE configuration file with RHEL installation option. Note that all paths used in this file must be placed in /var/lib/tftpboot directory
$ sudo cat /var/lib/tftpboot/pxelinux/pxelinux.cfg/default
DEFAULT menu.c32
TIMEOUT 100
PROMPT 0

MENU OCP PXE INSTALALTION Menu

LABEL bootstrap
  KERNEL http://${BASTION}/rhcos-4.4.3-x86_64-installer-kernel-x86_64
  APPEND ip=dhcp rd.neednet=1 initrd=http://${BASTION}:8080/rhcos-4.4.3-x86_64-installer-initramfs.x86_64.img console=tty0 console=ttyS0 coreos.inst=yes coreos.inst.install_dev=sda coreos.inst.image_url=http://${BASTION}:8080/rhcos-4.4.3-x86_64-metal.x86_64.raw.gz coreos.inst.ignition_url=http://${BASTION}:8080/bootstrap.ign

LABEL master
  KERNEL http://${BASTION}/rhcos-4.4.3-x86_64-installer-kernel-x86_64
  APPEND ip=dhcp rd.neednet=1 initrd=http://${BASTION}:8080/rhcos-4.4.3-x86_64-installer-initramfs.x86_64.img console=tty0 console=ttyS0 coreos.inst=yes coreos.inst.install_dev=sda coreos.inst.image_url=http://${BASTION}:8080/rhcos-4.4.3-x86_64-metal.x86_64.raw.gz coreos.inst.ignition_url=http://${BASTION}:8080/master.ign

LABEL worker
  KERNEL http://${BASTION}/rhcos-4.4.3-x86_64-installer-kernel-x86_64
  APPEND ip=dhcp rd.neednet=1 initrd=http://${BASTION}:8080/rhcos-4.4.3-x86_64-installer-initramfs.x86_64.img console=tty0 console=ttyS0 coreos.inst=yes coreos.inst.install_dev=sda coreos.inst.image_url=http://${BASTION}:8080/rhcos-4.4.3-x86_64-metal.x86_64.raw.gz coreos.inst.ignition_url=http://${BASTION}:8080/worker.ign

# Allow Necessary Services and Ports in Firewalld
$ sudo firewall-cmd --permanent --add-service={tftp,ftp,dns,dhcp}
$ sudo firewall-cmd --permanent --add-port={69/udp,4011/udp}
$ sudo firewall-cmd --reload
////////////     3 - pxe server configuration end   ////////////




////////////     4 - dhcp server configuration start   ////////////

$ cat /etc/dhcp/dhcpd.conf
option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

subnet 10.0.0.0 netmask 255.255.255.0 {
	option routers 10.0.0.254;
	range 10.0.0.2 10.0.0.253;

  class "pxeclients" {
      match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
      next-server 10.0.0.1;

      if option architecture-type = 00:07 {
        # filename "uefi/shim.efi";
      } else {
        filename "pxelinux/pxelinux.0";
      }
  }

  host bootstrap {
      hardware ethernet 52:54:60:11:11:11;
      fixed-address 10.0.0.2;
  }

  host master-0 {
      hardware ethernet 52:54:60:11:11:11;
      fixed-address 10.0.0.2;
  }

}
////////////     4 - dhcp server configuration end  ////////////




////////////     5 - http server configuration start  ////////////




////////////     6 - dns server configuration start  ////////////




////////////     7 - provisioning bootstrap start  ////////////
# creating kvm vm disk
$ sudo gunzip -k /artifacts/rhcos-${OCPVERSION}-x86_64-qemu.x86_64.qcow2.gz
$ sudo qemu-img create -f qcow2 -b /artifacts/rhcos-${OCPVERSION}-x86_64-qemu.x86_64.qcow2 /var/lib/libvirt/images/bootstrap.qcow2 100G

# create virtual machine
$ sudo virt-install --name=bootstrat  --ram=16384 --vcpus=4 --cpu host --clock hpet_present=yes --pxe --memballoon virtio --graphics vnc --console pty,target_type=serial --disk path=/var/lib/libvirt/images/bootstrap.qcow2,format=qcow2,bus=virtio --os-variant=rhel8.0 --network network=net-ocp4,model=virtio --boot menu=on -m 52:54:60:11:11:11 --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=/var/www/html/bootstrap.ign"
////////////     7 - provisioning bootstrap end  ////////////




////////////     8 - provisioning ocp cluster nodes  ////////////
-------------
