#!/bin/bash

source ocp4_install_env.sh

sudo sed -i -e 's/Listen 80/Listen 8080/g' /etc/httpd/conf/httpd.conf
sudo firewall-cmd --permanent --add-port=8080/tcp -q
sudo firewall-cmd --reload -q
sudo mkdir -p /var/www/html
echo "hola" > /tmp/index.html
sudo cp /tmp/index.html /var/www/html/index.html
sudo chcon -t http_port_t /var/www/html/*
sudo restorecon -FRv
sudo systemctl enable --now httpd -q
sudo systemctl restart httpd -q
sudo systemctl status httpd
