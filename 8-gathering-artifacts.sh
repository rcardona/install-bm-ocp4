#!/bin/bash

source ocp4_install_env.sh

# download artifacts
sudo mkdir -p /artifacts
cd /artifacts
sudo wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-linux.tar.gz
sudo mkdir -p /usr/local/bin
sudo tar xzfv openshift-install-linux.tar.gz -C /usr/local/bin
sudo wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-client-linux.tar.gz
sudo tar xzfv openshift-client-linux.tar.gz -C /usr/local/bin
sudo wget ${OCPREPORHCOS}/rhcos-${OCPVERSION}-x86_64-metal.x86_64.raw.gz
sudo wget ${OCPREPORHCOS}/rhcos-${OCPVERSION}-x86_64-installer-initramfs.x86_64.img
sudo wget ${OCPREPORHCOS}/rhcos-${OCPVERSION}-x86_64-installer-kernel-x86_64
sudo wget ${OCPREPORHCOS}/rhcos-${OCPVERSION}-x86_64-qemu.x86_64.qcow2.gz
sudo cp rhcos-${OCPVERSION}-x86_64-metal.x86_64.raw.gz /var/www/html
sudo cp rhcos-${OCPVERSION}-x86_64-installer-initramfs.x86_64.img /var/www/html
sudo cp rhcos-${OCPVERSION}-x86_64-installer-kernel-x86_64 /var/www/html
