#!/bin/bash

source ocp4_install_env.sh

sudo systemctl enable --now rpcbind
sudo systemctl enable --now nfs-server
sudo firewall-cmd --permanent --add-service=mountd -q
sudo firewall-cmd --permanent --add-service=nfs -q
sudo firewall-cmd --permanent --add-service=rpc-bind -q
sudo firewall-cmd --reload -q

sudo pvcreate /dev/${NFS_DEV}
sudo vgcreate ocpvg /dev/${NFS_DEV}
sudo lvcreate -l 100%FREE -n nfs ocpvg
sudo mkfs.xfs -q /dev/mapper/ocpvg-nfs
sudo echo "/dev/mapper/ocpvg-nfs  ${NFSROOT} xfs defaults    0 0" >> /etc/fstab
sudo mount -a
sudo mkdir ${NFSROOT}/pv-infra-registry
sudo mkdir ${NFSROOT}/pv-user-pvs
sudo chmod -R 0770 ${NFSROOT}
sudo echo "${NFSROOT}/pv-infra-registry *(rw,sync,no_root_squash)" >> /etc/exports
sudo echo "${NFSROOT}/pv-user-pvs *(rw,sync,no_root_squash)" >> /etc/exports
sudo echo "${NFSROOT}/pv-infra-registry *(rw,sync,no_root_squash)" >> /etc/exports
sudo echo "${NFSROOT}/pv-user-pvs *(rw,sync,no_root_squash)" >> /etc/exports


sudo systemctl start rpcbind
sudo systemctl start nfs-server
