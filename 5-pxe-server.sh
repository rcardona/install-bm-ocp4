#!/bin/bash

source ocp4_install_env.sh

# Configuration PXE server
sudo cp -r /usr/share/syslinux/* /var/lib/tftpboot
sudo mkdir -p /var/lib/tftpboot/pxelinux.cfg
sudo chmod -R 0777 /var/lib/tftpboot/pxelinux.cfg
# sudo semanage port -l |grep -i http
sudo chcon -t tftpdir_rw_t /var/lib/tftpboot/*
sudo restorecon -FRv /var/lib/tftpboot
sudo touch /var/lib/tftpboot/pxelinux.cfg/default

# now edit PXE configuration file with RHEL installation option. Note that all paths used in this file must be placed in /var/lib/tftpboot directory
sudo cat > /var/lib/tftpboot/pxelinux.cfg/default << EOF
DEFAULT menu.c32
TIMEOUT 300
PROMPT 0

MENU OCP PXE INSTALALTION Menu

LABEL bootstrap
  KERNEL http://${BASTION_IP}/rhcos-4.4.3-x86_64-installer-kernel-x86_64
  APPEND ip=dhcp rd.neednet=1 initrd=http://${BASTION_IP}:8080/rhcos-4.4.3-x86_64-installer-initramfs.x86_64.img console=tty0 console=ttyS0 coreos.inst=yes coreos.inst.install_dev=sda coreos.inst.image_url=http://${BASTION_IP}:8080/rhcos-4.4.3-x86_64-metal.x86_64.raw.gz coreos.inst.ignition_url=http://${BASTION_IP}:8080/bootstrap.ign

LABEL master
  KERNEL http://${BASTION_IP}/rhcos-4.4.3-x86_64-installer-kernel-x86_64
  APPEND ip=dhcp rd.neednet=1 initrd=http://${BASTION_IP}:8080/rhcos-4.4.3-x86_64-installer-initramfs.x86_64.img console=tty0 console=ttyS0 coreos.inst=yes coreos.inst.install_dev=sda coreos.inst.image_url=http://${BASTION_IP}:8080/rhcos-4.4.3-x86_64-metal.x86_64.raw.gz coreos.inst.ignition_url=http://${BASTION_IP}:8080/master.ign

LABEL worker
  KERNEL http://${BASTION_IP}/rhcos-4.4.3-x86_64-installer-kernel-x86_64
  APPEND ip=dhcp rd.neednet=1 initrd=http://${BASTION_IP}:8080/rhcos-4.4.3-x86_64-installer-initramfs.x86_64.img console=tty0 console=ttyS0 coreos.inst=yes coreos.inst.install_dev=sda coreos.inst.image_url=http://${BASTION_IP}:8080/rhcos-4.4.3-x86_64-metal.x86_64.raw.gz coreos.inst.ignition_url=http://${BASTION_IP}:8080/worker.ign
EOF

# Allow Necessary Services and Ports in Firewalld
sudo firewall-cmd --permanent --add-service={tftp,ftp}
sudo firewall-cmd --permanent --add-port={69/udp,4011/udp}
sudo firewall-cmd --reload -q
sudo systemctl enable --now tftp.service -q
sudo systemctl restart tftp.service -q
