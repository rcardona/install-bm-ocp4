#!/bin/bash

source ocp4_install_env.sh

# configuring

echo -e  "defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          300s
    timeout server          300s
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 20000
frontend openshift-api-server
    bind *:6443
    default_backend openshift-api-server
    mode tcp
    option tcplog
frontend machine-config-server
    bind *:22623
    default_backend machine-config-server
    mode tcp
    option tcplog
frontend ingress-http
    bind *:80
    default_backend ingress-http
    mode tcp
    option tcplog
frontend ingress-https
    bind *:443
    default_backend ingress-https
    mode tcp
    option tcplog
backend openshift-api-server
    balance source
    mode tcp
    server bootstrap ${BOOTSTRAP_IP}:6443 check
    server master-0 ${MASTER_0_IP}:6443 check
    server master-1 ${MASTER_1_IP}:6443 check
    server master-2 ${MASTER_2_IP}:6443 check
backend machine-config-server
    balance source
    mode tcp
    server bootstrap ${BOOTSTRAP_IP}:22623 check
    server master-0 ${MASTER_0_IP}:22623 check
    server master-1 ${MASTER_1_IP}:22623 check
    server master-2 ${MASTER_2_IP}:22623 check
backend ingress-http
    balance source
    mode tcp
    server worker-0 ${WORKER_0_IP}:80 check
    server worker-1 ${WORKER_1_IP}:80 check
    server worker-2 ${WORKER_2_IP}:80 check
backend ingress-https
    balance source
    mode tcp
    server worker-0 ${WORKER_0_IP}:443 check
    server worker-1 ${WORKER_1_IP}:443 check
    server worker-2 ${WORKER_2_IP}:443 check" | sudo tee /etc/haproxy/haproxy.cfg


sudo setsebool -P haproxy_connect_any 1
sudo restorecon -FRv /etc/haproxy
sudo firewall-cmd --permanent --add-service=http -q
sudo firewall-cmd --permanent --add-service=https -q
sudo firewall-cmd --permanent --add-port=6443/tcp -q
sudo firewall-cmd --permanent --add-port=22623/tcp -q
sudo firewall-cmd --reload -q
sudo systemctl enable --now haproxy -q
sudo systemctl restart haproxy -q
