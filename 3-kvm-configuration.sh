#!/bin/bash

source ocp4_install_env.sh

# check kvm capabilities
grep -E --color 'vmx|svm' /proc/cpuinfo

# create lvm to store vm images
# sudo lsblk
# sudo pvcreate /dev/sdb
# sudo vgextend /dev/rhel /dev/sdb
# sudo lvcreate -l 100%FREE -n lv-kvm /dev/rhel
# sudo mkfs.xfs /dev/rhel/lv-kvm
# sudo blkid
# sudo vim /etc/fstab
# sudo mount -a

# show active interfaces
sudo nmcli c s --active

# create network bridge
sudo nmcli c down ${BASTION_INSTALLATION_INTERFACE}

# create network definion file
sudo cat > /tmp/net-definition.xml << EOF
<network>
  <name>net-ocp4</name>
  <forward mode="bridge"/>
  <bridge name="ocp4-bridge" dev="${BASTION_INSTALLATION_INTERFACE}"/>
</network>
EOF

# define network
sudo virsh net-define /tmp/net-definition.xml
sudo virsh net-start net-ocp4
sudo virsh net-autostart net-ocp4
