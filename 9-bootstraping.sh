#!/bin/bash

# create a disk
# sudo qemu-img create -f raw -o size=20G /mnt/drives-kvm/rhel8-guest-pxe.img
# sudo qemu-img create -f qcow2 /mnt/drives-kvm/rhel8-pxe.qcow2 20G

virt-install -q --name="bootstrap" --vcpus=16384 --ram=4096 --cpu host --clock hpet_present=yes --noautoconsole --pxe --memballoon virtio --graphics vnc --console pty,target_type=serial --disk path=/var/lib/libvirt/images/bootstrap.qcow2,format=qcow2,bus=virtio --os-variant rhel8.0 --network network=ocp4,model=virtio --boot menu=on -m 54:52:00:01:02:03 --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=/tmp/bootstrap.ign"
