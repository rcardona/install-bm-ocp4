#!/bin/bash

source ocp4_install_env.sh

sudo cat > /etc/dhcp/dhcpd.conf << EOF
option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

subnet 10.0.0.0 netmask 255.255.255.0 {
	option routers 10.0.0.254;
	range 10.0.0.2 10.0.0.253;

  class "pxeclients" {
      match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
      next-server ${BASTION_IP};

      if option architecture-type = 00:07 {
        # filename "uefi/shim.efi";
      } else {
        filename "pxelinux.0";
      }
  }

  host bootstrap {
      hardware ethernet 52:54:60:11:11:11;
      fixed-address 10.0.0.2;
  }

  host master-0 {
      hardware ethernet 52:54:60:11:11:12;
      fixed-address 10.0.0.3;
  }

}
EOF

sudo firewall-cmd --permanent --add-service=dhcp
sudo firewall-cmd --reload
sudo systemctl enable --now dhcpd -q
sudo systemctl restart dhcpd -q
